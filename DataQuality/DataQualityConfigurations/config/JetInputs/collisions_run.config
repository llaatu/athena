# **********************************************************************
# $Id: collisions_run.config 748034 2021-10-18 13:44:45Z sawyer $
# **********************************************************************

############################################################
# JetInputs
############################################################
#############
# Output
#############

output top_level {
    output CaloTopoClusters {
        algorithm = WorstCaseSummary

        output AllClusters {
            output Expert {
            } # Expert

        } # AllClusters

        output CalBAR {
        } # CalBAR

        output CalECA {
        } # CalECA

        output CalECC {
        } # CalECC

    } # CaloTopoClusters
    output PFOs {
        algorithm = WorstCaseSummary

        output AllPFOs {
        } # AllPFOs

        output ChargedPFOs {
        } # ChargedPFOs

        output NeutralPFOs {
        } # NeutralPFOs

    } # PFOs
} # output top_level

######################
# Histogram Assessment
######################

dir CaloTopoClusters {
    algorithm = ClusterGatherData
    reference = CentrallyManagedReferences

    dir AllClusters {
        dir Expert {
            output = CaloTopoClusters/AllClusters/Expert

            hist AveLARQ {
                algorithm = ClusterChisq
                display = StatBox
            }
            hist AveTileQ {
                algorithm = ClusterChisq
                display = StatBox
            }
            hist BadLARQFrac {
                algorithm = ClusterChisq
                display = StatBox
            }
            hist EBadCells {
                algorithm = ClusterChisq
                display = StatBox
            }
            hist EngPos {
                algorithm = ClusterChisq
                display = StatBox
            }
            hist HotRat {
                algorithm = ClusterChisq
                display = StatBox
            }
            hist HotRat_vs_clusterPhi_vs_clusterEta {
                algorithm = ClusterChisq
                display = StatBox
            }
            hist clusterE_vs_clusterTime {
                algorithm = ClusterChisq
                display = StatBox
            }
            hist clusterIsol {
                algorithm = ClusterChisq
                display = StatBox
            }
            hist clusterTime {
                algorithm = ClusterChisq
                display = StatBox
            }
            hist nBadCells {
                algorithm = ClusterChisq
                display = StatBox
            }
            hist nBadCells_vs_clusterPhi_vs_clusterEta {
                algorithm = ClusterChisq
                display = StatBox
            }
            hist nCells {
                algorithm = ClusterChisq
                display = StatBox
            }
            hist nCells_vs_clusterPhi_vs_clusterEta {
                algorithm = ClusterChisq
                display = StatBox
            }
            hist vHotRatE {
                algorithm = ClusterChisq
                display = StatBox
            }
            hist vHotRatOcc {
                algorithm = ClusterChisq
                display = StatBox
            }
        } # Expert


        output = CaloTopoClusters/AllClusters

        hist clusterE {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist clusterET {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist clusterET_vs_clusterEta {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist clusterET_vs_clusterPhi {
            algorithm = ClusterBinsDiffFromStripMedian
            display = LogZ
            weight = 0.0
        }
        hist clusterET_vs_clusterPhi_vs_clusterEta {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist clusterE_vs_clusterEta {
           algorithm = ClusterBinsDiffFromStripMedian
           display = LogZ
           weight = 0.0
        }
        hist clusterE_vs_clusterPhi {
           algorithm = ClusterBinsDiffFromStripMedian
           display = LogZ
           weight = 0.0
        }
        hist clusterE_vs_clusterPhi_vs_clusterEta {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist clusterEta {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist clusterPhi {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist nClusters {
            algorithm = ClusterChisq
            display = StatBox
        }
    } # AllClusters


    dir CalBAR {
        output = CaloTopoClusters/CalBAR

        hist Thresh0BAROcc {
           algorithm = ClusterBinsDiffFromStripMedian
           display = LogZ
           weight = 0.0
        }
        hist Thresh1BAROcc {
           algorithm = ClusterBinsDiffFromStripMedian
           display = LogZ
           weight = 0.0
        }
        hist Thresh2BAROcc {
           algorithm = ClusterBinsDiffFromStripMedian
           display = LogZ
           weight = 0.0
        }
        hist Thresh3BAROcc {
           algorithm = ClusterBinsDiffFromStripMedian
           display = LogZ
           weight = 0.0
        }
        hist Thresh4BAROCc {
           algorithm = ClusterBinsDiffFromStripMedian
           display = LogZ
           weight = 0.0
        }
    } # CalBAR


    dir CalECA {
        output = CaloTopoClusters/CalECA

        hist Thresh0ECAOcc {
           algorithm = ClusterBinsDiffFromStripMedian
           display = LogZ
           weight = 0.0
        }
        hist Thresh1ECAOcc {
           algorithm = ClusterBinsDiffFromStripMedian
           display = LogZ
           weight = 0.0
        }
        hist Thresh2ECAOcc {
           algorithm = ClusterBinsDiffFromStripMedian
           display = LogZ
           weight = 0.0
        }
        hist Thresh3ECAOcc {
           algorithm = ClusterBinsDiffFromStripMedian
           display = LogZ
           weight = 0.0
        }
    } # CalECA


    dir CalECC {
        output = CaloTopoClusters/CalECC

        hist Thresh0ECCOcc {
           algorithm = ClusterBinsDiffFromStripMedian
           display = LogZ
           weight = 0.0
        }
        hist Thresh1ECCOcc {
           algorithm = ClusterBinsDiffFromStripMedian
           display = LogZ
           weight = 0.0
        }
        hist Thresh2ECCOcc {
           algorithm = ClusterBinsDiffFromStripMedian
           display = LogZ
           weight = 0.0
        }
        hist Thresh3ECCOcc {
           algorithm = ClusterBinsDiffFromStripMedian
           display = LogZ
           weight = 0.0
        }
        hist Thresh4ECCOcc {
           algorithm = ClusterBinsDiffFromStripMedian
           display = LogZ
           weight = 0.0
        }
    } # CalECC


} # CaloTopoClusters

dir PFOs {
    algorithm = ClusterGatherData
    reference = CentrallyManagedReferences

    dir AllPFOs {
        output = PFOs/AllPFOs

        hist nPFOs {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist pfoE {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist pfoE_vs_pfoPhi_vs_pfoEta {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist pfoEta {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist pfoMass {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist pfoPhi {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist pfoPhi_vs_pfoEta {
            algorithm = ClusterBinsDiffFromStripMedian
            display = LogZ
            weight = 0.0
        }
        hist pfoRapidity {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist pfopT {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist pfopT_vs_pfoPhi_vs_pfoEta {
            algorithm = ClusterChisq
            display = StatBox
        }
    } # AllPFOs


    dir ChargedPFOs {
        output = PFOs/ChargedPFOs

        hist ChpfoDenseEnv {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist ChpfoE {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist ChpfoE_vs_ChpfoPhi_vs_ChpfoEta {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist ChpfoE_vs_ChpfoPhi_vs_ChpfoExpE {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist ChpfoEta {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist ChpfoExpE {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist ChpfoMass {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist ChpfoPhi {
            algorithm = ClusterBinsDiffFromStripMedian
            display = LogZ
            weight = 0.0
        }
        hist ChpfoPhi_vs_ChpfoEta {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist ChpfopT {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist ChpfopT_vs_ChpfoPhi_vs_ChpfoEta {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist DenseEnvFlagE {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist DenseEnvFlagEtaPhi {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist nChPFOs {
            algorithm = ClusterChisq
            display = StatBox
        }
    } # ChargedPFOs


    dir NeutralPFOs {
        output = PFOs/NeutralPFOs

        hist NupfoAvgLarQ {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist NupfoBadLarQFrac {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist NupfoCenterLambda {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist NupfoE {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist NupfoEBadCells {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist NupfoEMProb {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist NupfoEPos {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist NupfoE_vs_NupfoPhi_vs_NupfoEta {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist NupfoEta {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist NupfoIsolation {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist NupfoMass {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist NupfoNBadCells {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist NupfoPhi {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist NupfoPhi_vs_NupfoEta {
            algorithm = ClusterBinsDiffFromStripMedian
            display = LogZ
            weight = 0.0
        }
        hist NupfoRapidity {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist NupfoSecondLambda {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist NupfoSecondR {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist NupfopT {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist NupfopT_vs_NupfoPhi_vs_NupfoEta {
            algorithm = ClusterChisq
            display = StatBox
        }
        hist nNuPFOs {
            algorithm = ClusterChisq
            display = StatBox
        }
    } # NeutralPFOs


} # PFOs

##############
# Algorithms
##############
# Your algorithms; NOT those appearing in the corresponding config/common/ file

algorithm ClusterGatherData {
    libname = libdqm_algorithms.so
    name = GatherData
    reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences
}

# Summary makers

algorithm ClusterBinwiseSummary {
  libname = libdqm_summaries.so
  name = BinwiseSummary
}

algorithm ClusterWorstCaseYellow {
  libname = libdqm_summaries.so
  name = WorstCaseYellow
}

algorithm ClusterBinsDiffFromStripMedian {
  libname = libdqm_algorithms.so
  SuppressFactor = 0
  SuppressRedFactor = 0
  name = BinsDiffFromStripMedian
  thresholds = CaloMon_BinsDiffFromStripMedian_threshold
  MaxPublish = 200
}

algorithm ClusterBinsDiffFromStripMedian_barrel {
  libname = libdqm_algorithms.so
  xmin = -1.5
  xmax = 1.5
  SuppressFactor = 0
  SuppressRedFactor = 0
  name = BinsDiffFromStripMedian
  thresholds = ClusterBinsDiffFromStripMedian_threshold
  MaxPublish = 200
}

algorithm ClusterBinsDiffFromStripMedian_endcapA {
  libname = libdqm_algorithms.so
  xmin = 1.5
  xmax = 4.9
  SuppressFactor = 0
  SuppressRedFactor = 0
  name = BinsDiffFromStripMedian
  thresholds = ClusterBinsDiffFromStripMedian_threshold
  MaxPublish = 200
}

algorithm ClusterBinsDiffFromStripMedian_endcapC {
  libname = libdqm_algorithms.so
  xmin = -4.9
  xmax = -1.5
  SuppressFactor = 0
  SuppressRedFactor = 0
  name = BinsDiffFromStripMedian
  thresholds = ClusterBinsDiffFromStripMedian_threshold
  MaxPublish = 200
}

algorithm ClusterChisq {
    libname = libdqm_algorithms.so
    name = GatherData&Chi2NDF
    thresholds = METChi2Thresh
    reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences
}

##############
# Thresholds
##############

# Your thresholds

thresholds ClusterBinsDiffFromStripMedian_threshold {
  limits MaxDeviation {
    warning = 25 
    error = 200
  }
}

